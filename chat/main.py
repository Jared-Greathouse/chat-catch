from fastapi import (
    FastAPI,
    WebSocket,
    WebSocketDisconnect,
)
from starlette.websockets import WebSocketState
from websocket_manager import ConnectionManager
from controllers.chatrooms import get_chatroom, upload_message_to_chatroom
from mongodb import (
    connect_to_mongo,
    close_mongo_connection,
    get_nosql_db,
    MONGODB_DB_NAME,
)
import pymongo
import logging
import json
from starlette.middleware.cors import CORSMiddleware
from api import router as api_router
from authenticator import authenticator

app = FastAPI()


@app.middleware("http")
async def add_cors_headers(request, call_next):
    response = await call_next(request)
    response.headers[
        "Access-Control-Allow-Origin"
    ] = "https://jared-greathouse.gitlab.io"
    response.headers["Access-Control-Allow-Credentials"] = "true"
    response.headers[
        "Access-Control-Allow-Methods"
    ] = "GET, POST, PUT, DELETE, OPTIONS"
    response.headers[
        "Access-Control-Allow-Headers"
    ] = "Content-Type, Authorization"
    return response


origins = ["https://jared-greathouse.gitlab.io"]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.include_router(api_router)
app.include_router(authenticator.router)
logger = logging.getLogger(__name__)


@app.on_event("startup")
async def startup_event():
    await connect_to_mongo()
    client = await get_nosql_db()
    db = client[MONGODB_DB_NAME]
    try:
        db.create_collection("accounts")
    except pymongo.errors.CollectionInvalid as e:
        logging.warning(e)
    try:
        db.create_collection("chatrooms")
    except pymongo.errors.CollectionInvalid as e:
        logging.warning(e)
    try:
        account_collection = db.accounts
        chatroom_collection = db.chatrooms
        account_collection.create_index(
            "username",
            name="username",
            unique=True,
        )
        chatroom_collection.create_index(
            "chatroom_name",
            name="chatroom_name",
            unique=True,
        )
    except pymongo.errors.CollectionInvalid as e:
        logging.warning(e)


@app.on_event("shutdown")
async def shutdown_event():
    await close_mongo_connection()


manager = ConnectionManager()


@app.websocket("/ws/{user_name}/{chatroom_name}")
async def websocket_endpoint(
    websocket: WebSocket,
    user_name,
    chatroom_name,
):
    await manager.connect(websocket, user_name, chatroom_name)
    try:
        chatroom = await get_chatroom(chatroom_name)
        data = json.dumps(
            {
                "type": "entrance",
                "new_chatroom_obj": chatroom,
            },
            default=str,
        )
        await manager.broadcast(data, user_name, chatroom_name)
        while True:
            if websocket.application_state == WebSocketState.CONNECTED:
                try:
                    data = await websocket.receive_text()
                    print("data-receive_text", data)
                except WebSocketDisconnect:
                    await manager.disconnect(user_name, chatroom_name)
                await upload_message_to_chatroom(data)
                logger.info(f"DATA RECEIVED: {data}")
                await manager.broadcast(data, user_name, chatroom_name)
            else:
                logger.warning(
                    f"{websocket.application_state},reconnecting..."
                )
                await manager.connect(websocket, user_name, chatroom_name)
    except Exception:
        if websocket.application_state == WebSocketState.CONNECTED:
            await manager.disconnect(user_name, chatroom_name)
