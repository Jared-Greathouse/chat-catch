from pydantic import BaseModel, Field
from typing import List, Optional
from bson import ObjectId
from datetime import datetime
from jwtdown_fastapi.authentication import Token


class User(BaseModel):
    username: str
    # hashed_password: str
    # salt: str
    # profile_pic_img_src: Optional[str]
    # favorites: List[str] = []
    # disabled: bool = False


class UserInDB(User):
    _id: ObjectId
    date_created: datetime = Field(default_factory=datetime.utcnow)


# MESSAGE MODELS
class Message(BaseModel):
    user: UserInDB
    content: str = None
    edited: bool


class MessageInDB(Message):
    _id: ObjectId
    message: Message
    timestamp: datetime = Field(default_factory=datetime.utcnow)


# CHATROOM MODELS
class Chatroom(BaseModel):
    chatroom_name: str
    members: Optional[List[UserInDB]] = []
    messages: Optional[List[MessageInDB]] = []
    last_pinged: datetime = Field(default_factory=datetime.utcnow)
    active: bool = False


class ChatroomInDB(Chatroom):
    _id: ObjectId
    chatroom_name: str
    members: Optional[List[UserInDB]] = []
    date_created: datetime = Field(default_factory=datetime.utcnow)


class DuplicateAccountError(ValueError):
    pass


class AccountIn(BaseModel):
    email: str
    password: str
    username: str


class AccountOut(BaseModel):
    id: str
    email: str
    username: str


class AccountOutWithPassword(AccountOut):
    hashed_password: str


class AccountForm(BaseModel):
    username: str
    password: str


class AccountToken(Token):
    account: AccountOut


class HttpError(BaseModel):
    detail: str
