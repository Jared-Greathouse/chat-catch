import { useState, useEffect } from "react";

const useFetchChatroom = (props) => {
  const [chatroom, setChatroom] = useState("");
  useEffect(() => {
    const fetchChatroom = async () => {
      const url = `${process.env.REACT_APP_CHAT_API_HOST}/api/chatrooms/${props.chatroomName}`;
      const response = await fetch(url);
      if (response.ok) {
        const data = await response.json();
        setChatroom(data);
      }
    };
    fetchChatroom();
  }, []);
  console.log("inside useFetchChatroom - ", chatroom);
  return chatroom;
};

export default useFetchChatroom;
