import pymongo
from pymongo import MongoClient
import logging
import os

MONGO_URL = os.environ.get("DATABASE_URL")
client = pymongo.MongoClient(MONGO_URL)

MONGODB_DB_NAME = "chat_catch"
MAX_CONNECTIONS_COUNT = 10
MIN_CONNECTIONS_COUNT = 1


class MongoDB:
    client: MongoClient = None


db = MongoDB()


async def connect_to_mongo():
    db.client = MongoClient(
        str(MONGO_URL),
        maxPoolSize=MAX_CONNECTIONS_COUNT,
        minPoolSize=MIN_CONNECTIONS_COUNT,
    )
    logging.info("connected to mongodb")


async def get_nosql_db() -> MongoClient:
    return db.client


async def close_mongo_connection():
    db.client.close()
    logging.info("closed mongo connection")


class Queries:
    @property
    def collection(self):
        db = client[self.DB_NAME]
        return db[self.COLLECTION]
