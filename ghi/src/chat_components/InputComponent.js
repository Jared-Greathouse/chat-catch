import Picker from "@emoji-mart/react";
import data from "@emoji-mart/data";
import Button from "react-bootstrap/Button";
import { useState, useEffect } from "react";

const InputComponent = (props) => {
  const [inputStr, setInputStr] = useState("");
  const [showPicker, setShowPicker] = useState(false);
  const [emojiObj, setEmojiObj] = useState("");

  let selectedEmoji = emojiObj.native;
  useEffect(() => {
    if (selectedEmoji) {
      setInputStr((inputStr) => inputStr + selectedEmoji);
      let added = true;
      if (added) {
      }
      added = false;
    }
  }, [selectedEmoji]);

  const handleSubmit = (event) => {
    event.preventDefault();
    props.handleSubmitMessage(inputStr);
    setInputStr("");
    setShowPicker(false);
  };

  return (
    <form>
      <div className="input-area">
        {showPicker && <Picker data={data} onEmojiSelect={setEmojiObj} />}
        <div className="input-wrapper">
          <input
            className="text-input"
            onChange={(e) => setInputStr(e.target.value)}
            type="text"
            value={inputStr}
          />
          <img
            className="emoji-icon"
            src="https://icons.getbootstrap.com/assets/icons/emoji-smile.svg"
            alt=""
            onClick={() => setShowPicker((val) => !val)}
          />
          <Button
            onClick={handleSubmit}
            type="submit"
            className="send-btn"
            variant="secondary"
          >
            {" "}
            Send{" "}
          </Button>
        </div>
      </div>
    </form>
  );
};

export default InputComponent;
