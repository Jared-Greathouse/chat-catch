from fastapi import (
    APIRouter,
)
from controllers.chatrooms import (
    insert_chatroom,
    get_chatroom,
    get_chatrooms,
    delete_chatroom,
    upload_message_to_chatroom,
)
from utils import format_ids
import json
from request_forms import (
    ChatroomCreateRequest,
    ChatroomMessageRequest,
)

router = APIRouter()


@router.post("/api/chatrooms")
async def create_chatroom(
    request: ChatroomCreateRequest,
):
    res = await insert_chatroom(request.username, request.chatroom_name)
    return res


@router.get("/api/chatrooms")
async def get_all_chatrooms():
    chatrooms = await get_chatrooms()
    return chatrooms


@router.get("/api/chatrooms/{chatroom_name}")
async def get_single_room(
    chatroom_name,
):
    chatroom = await get_chatroom(chatroom_name)
    formatted_chatroom = format_ids(chatroom)
    return formatted_chatroom


@router.delete("/api/chatrooms/{chatroom_name}")
async def delete_chatroom_db(
    chatroom_name: str,
):
    try:
        await delete_chatroom(chatroom_name)
    except Exception as e:
        return e
    return True


@router.put("/api/chatrooms/{chatroom_name}")
async def create_message(
    request: ChatroomMessageRequest,
):
    data = {
        "username": request.username,
        "chatroom_name": request.chatroom_name,
        "content": request.message,
    }
    res = await upload_message_to_chatroom(f"{json.dumps(data, default=str)}")
    return res
