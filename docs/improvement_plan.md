Improvement Plan

1. make full CRUD - delete messages and chatrooms
2. ~~sign in log statement - chat.js:26~~
3. ~~fix double "matt has entered chat" message~~
4. create chatroom and have it immediately populate
5. list of bad language will be censored
6. ~~chat.js:44 console log~~
7. ~~remove joeblow~~
8. [DOM] Input elements should have autocomplete attributes (suggested: "current-password"): (More info: https://goo.gl/9p2vKq) <input class=​"input" type=​"password" placeholder=​"**\*\*\*\***" id=​"password" name=​"password" value>​
9. Use chat catch image

Make all controllers and api classes into functions
fix the imports
rewrite unit tests
AirBNB style guide
research popular auth libraries
research popular front-end state management
make prettier
plan notifications feature
