from dotenv import load_dotenv

load_dotenv()
from fastapi.testclient import TestClient  # noqa
from unittest import TestCase  # noqa
from main import app  # noqa
from controllers.accounts import AccountQueries  # noqa


client = TestClient(app)

TestUser = {
    "email": "user1@email.com",
    "password": "password1",
    "username": "full_name1",
}

TestUserCreated = {
    "id": "639281736dc1541c96bba8c4",
    "email": "user1@email.com",
    "username": "full_name1",
}


def test_create_account():
    class TestUserQuery(TestCase):
        def create():
            pass

    app.dependency_overrides[AccountQueries.create] = (TestUserQuery,)
    print("Did i get here line 34")
    response = client.post("/api/accounts", json=TestUser)
    print(response.json())
    assert response.status_code == 200
    assert len(response.json()["account"]["id"]) == len(
        TestUserCreated["id"]
    )  # noqa
    assert response.json()["account"]["email"] == TestUserCreated["email"]
    assert (
        response.json()["account"]["username"] == TestUserCreated["username"]
    )  # noqa
    app.dependency_overrides = {}
