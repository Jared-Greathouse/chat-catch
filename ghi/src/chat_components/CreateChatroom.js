import { useState } from "react";
import Button from "react-bootstrap/Button";

const CreateChatroom = (props) => {
  const [createdChatroom, setCreatedChatroom] = useState("");
  const handleCreateChatroom = async () => {
    const data = {
      username: props.activeUser,
      chatroom_name: createdChatroom,
    };
    const url = `${process.env.REACT_APP_CHAT_API_HOST}/api/chatrooms`;
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(url, fetchConfig);
    if (response.ok) {
      console.log("Created Chatroom!");
      setCreatedChatroom("");
    }
  };
  return (
    <form>
      <input
        className="add-chat-room"
        onChange={(e) => setCreatedChatroom(e.target.value)}
        type="text"
        placeholder="Create Chatroom"
        value={createdChatroom}
      />
      <Button
        onClick={handleCreateChatroom}
        className="create-room-btn"
        variant="secondary"
      >
        Create
      </Button>
    </form>
  );
};

export default CreateChatroom;
