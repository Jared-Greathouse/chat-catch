import { Link } from "react-router-dom";

const LoginReminder = () => {
  return (
    <div className="window-wrapper">
      <p>Chat Catch</p>
      <div className="reminder-message">
        Sorry, you need to log in to see the chatrooms!
        <div>
          <Link className="login-here" to="/">
            Login here.
          </Link>
        </div>
      </div>
    </div>
  );
};

export default LoginReminder;
