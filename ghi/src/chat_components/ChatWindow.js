import React, { useRef, useEffect } from "react";

export default function ChatWindow(props) {
  const messagesEndRef = useRef(null);
  useEffect(() => {
    messagesEndRef.current?.scrollIntoView();
  }, [props.messages]);
  return (
    <div className="chat-list">
      {props.messages.map(({ username, content }, index) => {
        return (
          <option className="chat-text" key={index}>
            {`${username}: ${content}`}
          </option>
        );
      })}
      <div ref={messagesEndRef}></div>
    </div>
  );
}
