import { useState, useEffect } from "react";

const useFetchChatrooms = () => {
  const [chatrooms, setChatrooms] = useState([]);
  useEffect(() => {
    const fetchChatrooms = async () => {
      const url = `${process.env.REACT_APP_CHAT_API_HOST}/api/chatrooms`;
      const response = await fetch(url);
      if (response.ok) {
        const data = await response.json();
        setChatrooms(data);
      }
    };
    fetchChatrooms();
  }, []);
  return chatrooms;
};

export default useFetchChatrooms;
