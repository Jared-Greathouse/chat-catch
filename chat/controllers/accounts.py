from models import AccountIn, AccountOut, AccountOutWithPassword
from mongodb import Queries


class AccountQueries(Queries):
    DB_NAME = "chat_catch"
    COLLECTION = "accounts"

    def get(self, email: str):
        user = self.collection.find_one({"email": email})
        user["id"] = str(user["_id"])
        return AccountOutWithPassword(**user)

    def create(self, info: AccountIn, hashed_password: str):
        props = info.dict()
        print("props-----", props)
        props["hashed_password"] = hashed_password
        try:
            self.collection.insert_one(props)
        except Exception as e:
            print("this is error, oops.......", e)
        props["id"] = str(props["_id"])
        return AccountOutWithPassword(**props)

    def fetch_all_accounts(self):
        accounts = []
        props = self.collection.find({})
        for document in props:
            document["id"] = str(document["_id"])
            accounts.append(AccountOut(**document))
            print(document)
        return accounts
