import useFetchChatroom from "../hooks/useFetchChatroom";
import ChatWindow from "./ChatWindow";

const SelectChatroom = (props) => {
  const chatroom = useFetchChatroom(props.chatroomName);
  return <ChatWindow messages={chatroom.messages} />;
};

export default SelectChatroom;
