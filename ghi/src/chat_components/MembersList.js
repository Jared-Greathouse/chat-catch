const MembersList = (props) => {
  return (
    <div className="members-list">
      <ul className="members-list-title">
        Members
        {props.users?.map((user, index) => {
          return (
            <li className="member-name-in-list" key={index}>
              {user}
            </li>
          );
        })}
      </ul>
    </div>
  );
};

export default MembersList;
