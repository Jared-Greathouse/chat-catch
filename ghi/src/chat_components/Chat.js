import React, { useState, useEffect } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "./Chat.css";
import { useNavigate } from "react-router-dom";
import {
  useToken,
  useAuthContext,
  getUsername,
} from "../auth_components/GetToken";
import ChatWindow from "./ChatWindow";
import ChatroomList from "./ChatroomList";
import CreateChatroom from "./CreateChatroom";
import useFetchChatrooms from "../hooks/useFetchChatrooms";
import LogoutButton from "../auth_components/LogoutButton";
import LoginReminder from "../auth_components/LoginReminder";
import InputComponent from "./InputComponent";
import MembersList from "./MembersList";

function Chat(props) {
  const { token } = useAuthContext();
  const chatrooms = useFetchChatrooms();
  const [users, setUsers] = useState([]);
  const [selectedChatroom, setSelectedChatroom] = useState("");
  const [getMessages, setGetMessages] = useState([]);
  const [activeUser, setActiveUser] = useState("");
  const [ws, setWs] = useState(null);

  useEffect(() => {
    const name = getUsername();
    setActiveUser(name);
  }, []);

  const handleSelectedChatroom = (event) => {
    event.preventDefault();
    const chatroom = event.target.value;
    setSelectedChatroom(chatroom);
    connectToWebSocket(chatroom);
  };

  const connectToWebSocket = (selectedChatroom) => {
    const websocket = new WebSocket(
      `${process.env.REACT_APP_CHAT_WEBSOCKET_HOST}/ws/${activeUser}/${selectedChatroom}`
    );
    websocket.onopen = () => {};
    websocket.onmessage = (event) => {
      const message = JSON.parse(event.data);
      if (message.hasOwnProperty("type") && message.type === "entrance") {
        const chatroom = message.new_chatroom_obj;
        const messages = chatroom.messages;
        const members = chatroom.members;
        setUsers(members);
        setGetMessages([...messages]);
      } else {
        const username = message.user_name;
        const content = message.content;
        const messageBody = {
          username: username,
          content: content,
        };
        setGetMessages((current) => [...current, messageBody]);
      }
    };
    websocket.onclose = () => {};
    websocket.onerror = (error) => {
      console.log("---On Error", error.message);
      websocket.close();
    };
    setWs(websocket);
  };

  const handleSubmitMessage = (inputStr) => {
    const data = {
      user_name: activeUser,
      chatroom_name: selectedChatroom,
      content: inputStr,
    };
    ws.send(JSON.stringify(data));
  };

  const navigate = useNavigate();
  const [, , logout] = useToken();
  const handleLogout = async (e) => {
    e.preventDefault();
    logout();
    navigate("/");
  };

  if (token) {
    return (
      <div>
        <div className="window-wrapper">
          <div className="window-title">
            <div className="app-title">
              <div>Chat Catch</div>
            </div>
            <div className="expand">
              <i className="fa fa-expand"></i>
            </div>
          </div>
          <div className="window-area">
            <MembersList users={users} />
            <div className="chat-area">
              <div className="chat-area-title">
                <b>Current Room: </b>
                <b> {selectedChatroom} </b>
              </div>
              {<ChatWindow messages={getMessages} />}
              <InputComponent handleSubmitMessage={handleSubmitMessage} />
            </div>
            <div className="right-tabs">
              <ul className="tabs-container">
                <div className="title">
                  <b>Your Chatrooms</b>
                </div>
              </ul>
              <ChatroomList
                chatrooms={chatrooms}
                onSelectedChatroom={handleSelectedChatroom}
              />
              <CreateChatroom activeUser={activeUser} />
              <LogoutButton handleLogout={handleLogout} />
            </div>
          </div>
        </div>
      </div>
    );
  } else {
    return <LoginReminder />;
  }
}

export default Chat;
