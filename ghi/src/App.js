import React from "react";
import Chat from "./chat_components/Chat";
import Login from "./auth_components/Login";
import Register from "./auth_components/Register";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import "./App.css";
import { AuthProvider, useToken } from "./auth_components/GetToken";

const env = process.env.NODE_ENV || "development";
const baseName = env === "development" ? "" : "/chat-catch/";

function GetToken() {
  // Get token from JWT cookie (if already logged in)
  useToken();
  return null;
}

function App() {
  return (
    <BrowserRouter basename={baseName}>
      <AuthProvider>
        <GetToken />
        <div className="container">
          <Routes>
            <Route path="/" element={<Login />} />
            <Route path="register/" element={<Register />} />
            <Route path="chats/" element={<Chat />} />
          </Routes>
        </div>
      </AuthProvider>
    </BrowserRouter>
  );
}

export default App;
