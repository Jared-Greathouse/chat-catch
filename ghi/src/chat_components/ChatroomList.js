import React from "react";
import { Link } from "react-router-dom";

export default function ChatroomList(props) {
  return (
    <div className="chatroom-list">
      <ul>
        {props.chatrooms?.map(({ _id, chatroom_name }) => {
          return (
            <li onClick={props.onSelectedChatroom} key={_id}>
              <Link value={chatroom_name} style={{ textDecoration: "none" }}>
                <option className="chatroom-name-list">{chatroom_name}</option>
              </Link>
            </li>
          );
        })}
      </ul>
    </div>
  );
}
