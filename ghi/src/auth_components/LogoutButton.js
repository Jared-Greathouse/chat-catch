import { Link } from "react-router-dom";
import Button from "react-bootstrap/Button";

const LogoutButton = (props) => {
  return (
    <Link to="">
      <Button
        onClick={props.handleLogout}
        className="logout-btn"
        variant="outline-secondary"
      >
        Logout
      </Button>
    </Link>
  );
};

export default LogoutButton;
